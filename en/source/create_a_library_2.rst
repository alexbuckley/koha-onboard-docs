.. include:: images.rst

Create a library outcome
_____________________________

This screen indicates whether the library was created successfully.

.. figure:: /images/create_library_screen_2.*
   :alt: Create a library outcome

   Create a library outcome

1. **Create library message:** Indicates whether the library was created successfully
2. **Create more libraries:** If more libraries are required, or changes need to be made to this freshly created library, go to Administration->Libraries and groups
3. Click the *Add a patron category* button to go to the next step.
