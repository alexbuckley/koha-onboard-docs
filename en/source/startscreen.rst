.. include:: images.rst



Onboarding Tool
=======================


**Start Screen**


To get started with the onboarding tool click the *Start setting up my Koha* button.

.. figure:: /images/startscreen.*
   :alt: Onboarding tool start startscreen

   Onboarding tool start screen
