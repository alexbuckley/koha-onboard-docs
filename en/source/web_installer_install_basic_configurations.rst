.. include:: images.rst

Ready for basic configurations
__________________________________

This screen leads onto installing basic configurations neccisary to use Koha.

.. figure:: images/install_basic_configurations.*
   :alt: Ready for basic configurations

   Ready for basic configurations

Click the *Install Basic Configuration Settings* button
