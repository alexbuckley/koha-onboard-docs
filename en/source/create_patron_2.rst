.. include:: images.rst

Create a patron outcome
__________________________

This screen indicates whether the patron account was created successfully.

.. figure:: images/create_patron_2.*
   :alt: Create patron outcome

   Create patron outcome


1. **Create patron message:** This indicates whether the patron account was created successfully
2. **Path to create patron:** More patrons can be created by going to Patrons-> New patron
   Modify the new superlibrarian user by searching for the patron, then clicking on it for details.
3. Click the *Add an item type* button to go to the next step.
