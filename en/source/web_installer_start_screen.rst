.. include:: images.rst

Web Installer
==============

**Installer Start screen**


This is the first screen of the web installer.

.. figure:: images/installer_start_screen.*
   :alt: Web installer start screen

   Web installer start screen

1. *Language picker* dropdown box: This is specifying the language you want Koha to be in.

.. Note::
	Installing other languages that have translations for the installer (DE or FR, for example) will allow those languages to be used for the web installer.

2. Click the *Next* button to load the next stage of the web installer.
